class Parent():
    def __init__(self, last_name, eye_color):
        print "Parent Constructor Called"
        self.last_name = last_name
        self.eye_color = eye_color

    def show_last_name(self):
        return "Last Name - " + self.last_name

    def show_eye_color(self):
        return "Eye Color - " + self.eye_color

class Child(Parent):
    def __init__(self, last_name, eye_color, number_of_toys):
        print "Child Constructor Called"
        Parent.__init__(self, last_name, eye_color)
        self.number_of_toys = number_of_toys

    # Method Overriding in programming - the sub-class Child has ability to
    # override method that it inherited from its parent class.
    def show_last_name(self):
        return "Last Name - " + self.last_name

    def show_eye_color(self):
        return "Eye Color - " + self.eye_color

    def show_number_of_toys(self):
        return "Number of toys - " + str(self.number_of_toys)
