import pytest
from class_lessons.inheritance import Parent
from class_lessons.inheritance import Child


def test_parent():
    billy_cyrus = Parent("Cyrus", "blue")
    assert billy_cyrus.last_name == "Cyrus"
    assert billy_cyrus.eye_color == "blue"


def test_parent_show_last_name():
    billy_cyrus = Parent("Cyrus", "blue")
    assert billy_cyrus.show_last_name() == "Last Name - Cyrus"


def test_parent_show_eye_color():
    billy_cyrus = Parent("Cyrus", "blue")
    assert billy_cyrus.show_eye_color() == "Eye Color - blue"


def test_child():
    miley_cyrus = Child("Cyrus", "brown", 5)
    assert miley_cyrus.last_name == "Cyrus"
    assert miley_cyrus.eye_color == "brown"
    assert miley_cyrus.number_of_toys == 5


def test_child_show_last_name():
    miley_cyrus = Child("Cyrus", "brown", 5)
    assert miley_cyrus.show_last_name() == "Last Name - Cyrus"


def test_child_show_eye_color():
    miley_cyrus = Child("Cyrus", "brown", 5)
    assert miley_cyrus.show_eye_color() == "Eye Color - brown"


def test_child_show_number_of_toys():
    miley_cyrus = Child("Cyrus", "brown", 5)
    assert miley_cyrus.show_number_of_toys() == "Number of toys - 5"
