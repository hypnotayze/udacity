import urllib


def read_text(file_to_read):
    quotes = open(file_to_read)
    contents_of_file = quotes.read()
    quotes.close()
    print contents_of_file
    return contents_of_file


def check_profanity(url, text_to_check):
    """
    The generalized format of url is: http://www.wdylike.appspot.com/?q=QUERY,
    where you replace QUERY with the content of your query. For example:
    http://www.wdylike.appspot.com/?q=shit
    """
    connection = urllib.urlopen(url + text_to_check)
    output = connection.read()
    connection.close()
    message = ""
    if output == "true":
        message = "Profanity Alert!!"
    elif output == "false":
        message = "This document has no curse words!"
    else:
        message = "Could not scan the document properly."
    print message
    return message
