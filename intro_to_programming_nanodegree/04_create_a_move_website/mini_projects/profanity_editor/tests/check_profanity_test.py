import pytest
import os
from profanity_editor.check_profanity import read_text
from profanity_editor.check_profanity import check_profanity


def test_read_text():
    current_location = os.getcwd() + "/movie_quotes.txt"
    content = open(current_location)
    result = content.read()
    content.close()
    assert read_text(current_location) == result


def test_check_profanity_1():
    url = "http://www.wdylike.appspot.com/?q="
    text_to_check = "Don't give me shit"
    assert check_profanity(url, text_to_check) == "Profanity Alert!!"


def test_check_profanity_2():
    url = "http://www.wdylike.appspot.com/?q="
    current_location = os.getcwd() + "/movie_quotes.txt"
    content = open(current_location)
    text_to_check = content.read()
    content.close()
    assert check_profanity(url, text_to_check) == \
        "This document has no curse words!"
