import os
from string import digits


def rename_files(path):
    # 1. step: Get file names from a folder
    file_list = os.listdir(path)
    # 2. step: for each file, rename filename
    os.chdir(path)
    for file_name in file_list:
        os.rename(file_name, file_name.translate(None, digits))
    os.chdir(path)

path = "/home/kaminska/workspace/udacity/intro_to_programming_nanodegree/\
04_create_a_move_website/mini_projects/secret_message/prank"
rename_files(path)
