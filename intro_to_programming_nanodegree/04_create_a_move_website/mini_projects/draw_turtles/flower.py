import turtle


def draw_petal(some_turtle):
    some_turtle.forward(100)
    some_turtle.right(30)
    some_turtle.forward(100)
    some_turtle.right(150)
    some_turtle.forward(100)
    some_turtle.right(30)
    some_turtle.forward(100)


def draw_flower():
    window = turtle.Screen()
    # create the turtle Ninja - draws a square
    nice = turtle.Turtle()
    nice.shape("turtle")
    nice.color("blue")
    nice.speed(0)
    for x in range(45):
        draw_petal(nice)
        nice.left(202)
    nice.right(90)
    nice.forward(300)
    window.exitonclick()


draw_flower()
