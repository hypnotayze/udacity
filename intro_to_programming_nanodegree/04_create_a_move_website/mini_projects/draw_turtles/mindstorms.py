import turtle


def draw_square(some_turtle):
    for x in range(4):
        some_turtle.forward(100)
        some_turtle.right(90)


def draw_rectriangular_triangle(some_turtle):
    some_turtle.forward(-100)
    some_turtle.right(90)
    some_turtle.forward(100)
    some_turtle.right(45)
    some_turtle.goto(0, 0)


def draw_art():
    # our shapes will be in red window screen:
    window = turtle.Screen()
    window.bgcolor("red")
    # create the turtle Ninja - draws a square
    ninja = turtle.Turtle()
    ninja.shape("turtle")
    ninja.color("purple")
    ninja.speed(1)
    draw_square(ninja)
    # create the turtle Angie - draws a circle
    angie = turtle.Turtle()
    angie.shape("arrow")
    angie.color("green")
    angie.circle(100)
    # create the turtle Tria - draws rectangular triangle
    tria = turtle.Turtle()
    tria.shape("circle")
    draw_rectriangular_triangle(tria)
    # able to close window any time you click on it:
    window.exitonclick()


draw_art()
