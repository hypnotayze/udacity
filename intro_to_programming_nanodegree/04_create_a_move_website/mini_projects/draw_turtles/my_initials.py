import turtle


def letter_j(some_turtle):
    some_turtle.forward(50)
    some_turtle.right(90)
    some_turtle.forward(500)
    some_turtle.right(90)
    some_turtle.forward(50)
    some_turtle.right(100)
    some_turtle.forward(300)
    some_turtle.right(80)


def letter_t(some_turtle):
    some_turtle.forward(50)
    some_turtle.right(180)
    some_turtle.forward(25)
    some_turtle.left(90)
    some_turtle.forward(500)


def draw_initials():
    window = turtle.Screen()
    jt = turtle.Turtle()
    turtle.setworldcoordinates(-100, -800, 300, 400)
    jt.shape("turtle")
    jt.color("blue")
    x_start, y_start = jt.pos()
    letter_j(jt)
    # changing possition to draw letter "T"
    jt.penup()
    jt.goto(x_start + 100, y_start)
    jt.pendown()

    letter_t(jt)
    window.exitonclick()


draw_initials()
