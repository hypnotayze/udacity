import turtle


def draw_square(some_turtle):
    for x in range(4):
        some_turtle.forward(100)
        some_turtle.right(90)


def draw_stellar():
    window = turtle.Screen()
    window.bgcolor("purple")
    # create the turtle Ninja - draws a square
    nice = turtle.Turtle()
    nice.color("yellow")
    x = 0
    while x < 360:
        draw_square(nice)
        nice.right(10)
        x += 10
    window.exitonclick()


draw_stellar()
