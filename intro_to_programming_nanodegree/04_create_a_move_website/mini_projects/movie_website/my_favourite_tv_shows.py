import tv_shows
import media


# First step is to make instances of class TvShows with 5 arguments: Show name,
# show seasons, image url, youtube trailer url and short show desription.
# Then we make list with all this instances and use it as argument to call
# function open_tv_shows_page() from module tv_shows which we import above.
breaking_bad = media.TvShows("Breaking Bad", "Seasons 1-5 (2008-2013)",
                             "http://www.impawards.com/tv/posters/" +
                             "breaking_bad_xlg.jpg",
                             "https://www.youtube.com/watch?v=HhesaQXLuRY",
                             "A high school chemistry teacher diagnosed " +
                             "with inoperable lung cancer turns to " +
                             "manufacturing and selling methamphetamine in " +
                             "order to secure his family's future.")

hannibal = media.TvShows("Hannibal", "Seasons 1-3 (2013-2015)",
                         "http://s3.amazonaws.com/seat42faws1/wp-content/" +
                         "uploads/2013/03/12212133/Hannibal-Poster-TV.jpg",
                         "https://www.youtube.com/watch?v=Sx9bjEfzV_s",
                         "Explores the early relationship between the " +
                         "renowned psychiatrist and his patient, a young " +
                         "FBI criminal profiler, who is haunted by his " +
                         "ability to empathize with serial killers.")

true_detective = media.TvShows("True Detective", "Seasons 1-2 (2014-)",
                               "https://upload.wikimedia.org/wikipedia/en/" +
                               "7/7f/TrueDetectiveDVDCover.jpg",
                               "https://www.youtube.com/watch?v=fVQUcaO4AvE",
                               "Seasonal anthology series in which police " +
                               "investigations unearth the personal and " +
                               "professional secrets of those involved, " +
                               "both within and outside the law.")

greys_anatomy = media.TvShows("Grey's Anatomy", "Seasons 1-13 (2005-)",
                              "http://www.movieposter.com/posters/archive/" +
                              "main/91/MPW-45549",
                              "https://www.youtube.com/watch?v=4FC8krII948",
                              "A drama centered on the personal and " +
                              "professional lives of five surgical interns " +
                              "and their supervisors.")

prison_break = media.TvShows("Prison Break", "Seasons 1-4 (2005-2009)",
                             "https://s-media-cache-ak0.pinimg.com/" +
                             "originals/4b/47/b9/4b47b9ee06a4acd920e14c" +
                             "688ddc7b46.jpg",
                             "https://www.youtube.com/watch?v=AL9zLctDJaU",
                             "Due to a political conspiracy, an innocent " +
                             "man is sent to death row and his only hope " +
                             "is his brother, who makes it his mission to " +
                             "deliberately get himself sent to the same " +
                             "prison in order to break the both of them " +
                             "out, from the inside.")

friends = media.TvShows("Friends", "Seasons 1-10 (1994-2004)",
                        "https://s-media-cache-ak0.pinimg.com/736x/1f/08/" +
                        "9d/1f089d74ba51c8cfcfcda274d10eb946--friends--" +
                        "friends-tv-show.jpg",
                        "https://www.youtube.com/watch?v=hDNNmeeJs1Q",
                        "Follows the personal and professional lives of six " +
                        "20 to 30-something-year-old friends living in " +
                        "Manhattan.")


shows = [greys_anatomy, friends, prison_break, hannibal, breaking_bad,
         true_detective]
tv_shows.open_tv_shows_page(shows)
