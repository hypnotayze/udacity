// for loop used to loop over the array of donuts
var donuts = ["jelly donut", "chocolate donut", "glazed donut"];

for (var i = 0; i < donuts.length; i++) {
  donuts[i] += " hole";
  donuts[i] = donuts[i].toUpperCase();
}

console.log(donuts);

// but we can use method .forEach(function(element, index, array)):
var donuts = ["jelly donut", "chocolate donut", "glazed donut"];
// donut in printDonuts corresponds to each donut element in the array
// there's no need for index variables here or the array parameter so,
// we can leave those out
function printDonuts(donut) {
  donut += " hole";
  donut = donut.toUpperCase();
  console.log(donut);
}

donuts.forEach(printDonuts);

// when printDonuts is never really going to be use anywhere else in our code
// we could define it as an inline function expresion:
var donuts = ["jelly donut", "chocolate donut", "glazed donut"];

donuts.forEach(function(donut) {
  donut += " hole";
  donut = donut.toUpperCase();
  console.log(donut);
});

// With the map() method, I can take an array, perform some operation on
// each element of the array, and return a new array:
var donuts = ["jelly donut", "chocolate donut", "glazed donut"];
var newDonuts = donuts.map(function(donut) {
  donut += " hole";
  donut = donut.toUpperCase();
  return donut;
});
console.log(donuts);
console.log(newDonuts);
