
function helloCat(callbackFunc, num) {
  return "Hello " + callbackFunc(num);
}

console.log(helloCat(function catSays(num) {
  var catMessage = "";
  for (var i = 0; i < num; i++) {
    catMessage += "meow ";
  }
  return catMessage;
}, 3))
