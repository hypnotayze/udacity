import pytest
from lib import answer_for_blank
from lib import how_many_wrong_guesses
from lib import check_answer
from lib import check_answer_with_guesses
from lib import play_level
from lib import check_level
from lib import select_level
from lib import order_of_levels
from lib import play_game
from mock import patch


def test_answer_for_blank():
    answers_dictionary = {
        "___1___": "function",
        "___2___": "arguments",
        "___3___": "None",
        "___4___": "list",
    }
    assert answer_for_blank("___1___", answers_dictionary) == "function"


def test_answer_for_blank_returns_none():
    answers_dictionary = {
        "___1___": "function",
        "___2___": "arguments",
        "___3___": "None",
        "___4___": "list",
    }
    assert answer_for_blank("word", answers_dictionary) is None


def test_how_many_wrong_guesses():
    with patch('lib.quiz_game.get_input', return_value="1") as result:
        assert how_many_wrong_guesses() == 1


def test_how_many_wrong_guesses_with_one_wrong_value():
    with patch('lib.quiz_game.get_input', side_effect=["1.2", "1"]) as result:
        assert how_many_wrong_guesses() == 1


def test_how_many_wrong_guesses_with_zero():
    with patch('lib.quiz_game.get_input', side_effect=["0", "2"]) as result:
        assert how_many_wrong_guesses() == 2


def test_check_answer():
    right_answer = "function"
    assert check_answer("function", right_answer) == right_answer
    assert check_answer("!!function>>", right_answer) == right_answer
    right_answer = "None"
    assert check_answer("NOne", right_answer) == right_answer


def test_check_answer_raises_exeption():
    right_answer = "procedure"
    with pytest.raises(Exception):
        check_answer("function", right_answer)


def test_check_answer_with_guesses():
    answered_blanks = []
    message = "Fill blank "
    with patch('lib.quiz_game.get_input', return_value="function") as result:
        assert check_answer_with_guesses(1, message, "___1___", "function",
                                         answered_blanks) == 1
        assert answered_blanks == ["___1___"]


def test_check_answer_with_guesses_2():
    answered_blanks = []
    message = "Fill blank "
    inputs = ["None", "function"]
    with patch('lib.quiz_game.get_input', side_effect=inputs) as result:
        assert check_answer_with_guesses(1, message, "___1___", "function",
                                         answered_blanks) == 0
        assert answered_blanks == ["___1___"]


def test_check_answer_with_guesses_3():
    answered_blanks = []
    message = "Fill blank "
    inputs = ["None", "None"]
    with patch('lib.quiz_game.get_input', side_effect=inputs) as result:
        assert check_answer_with_guesses(1, message, "___1___", "function",
                                         answered_blanks) == -1
        assert answered_blanks == []


def test_play_level_user_win():
    sample = '''
A ___1___ is created with the def keyword. You specify the inputs a ___1___
takes by adding ___2___ separated by commas between the parentheses. ___1___s
by default return ___3___ if you don't specify the value to return. ___2___ can
be standard data types such as string, number, dictionary, tuple, and ___4___
or can be more complicated such as objects and lambda functions.'''
    answers_dictionary = {
        "___1___": "function",
        "___2___": "arguments",
        "___3___": "None",
        "___4___": "list",
    }
    answers = ["1", "function", "arguments", "None", "list"]
    message = "Fill blank "
    with patch('lib.quiz_game.get_input', side_effect=answers) as result:
        assert play_level(sample, answers_dictionary, message) == \
            "All answers are good!"


def test_play_level_user_lose():
    sample = '''
A ___1___ is created with the def keyword. You specify the inputs a ___1___
takes by adding ___2___ separated by commas between the parentheses. ___1___s
by default return ___3___ if you don't specify the value to return. ___2___ can
be standard data types such as string, number, dictionary, tuple, and ___4___
or can be more complicated such as objects and lambda functions.'''
    answers_dictionary = {
        "___1___": "function",
        "___2___": "arguments",
        "___3___": "None",
        "___4___": "list",
    }
    answers = ["3", "wrong", "not_right", "sth", "arguments", "None", "list"]
    message = "Fill blank "
    with patch('lib.quiz_game.get_input', side_effect=answers) as result:
        assert play_level(sample, answers_dictionary, message) == \
            "You used all wrong guesses, you lose."


def test_check_level_raises_exception():
    levels = ["easy", "medium", "hard"]
    with pytest.raises(Exception):
        check_level("slow", levels)


def test_check_level():
    levels = ["easy", "medium", "hard"]
    assert check_level("easy", levels) == "easy"


def test_select_level_two_wrong_levels_and_last_is_good():
    levels = ["easy", "medium", "hard"]
    with patch('lib.quiz_game.get_input', side_effect=["slow", "I", "hard"]) \
            as result:
        assert select_level(levels) == "hard"


def test_select_level():
    levels = ["easy", "medium", "hard"]
    with patch('lib.quiz_game.get_input', return_value="easy") as result:
        assert select_level(levels) == "easy"


def test_order_of_levels():
    keyorder = ["easy", "medium", "hard"]
    all_levels = {
        "easy": ("easy_string", "easy_answers"),
        "medium": ("medium_string", "medium_answers"),
        "hard": ("hard_string", "hard_answers"),
    }
    assert order_of_levels(all_levels, keyorder) == keyorder


def test_play_game_easy_user_win():
    easy_string = '''
A ___1___ is created with the def keyword. You specify the inputs a ___1___
takes by adding ___2___ separated by commas between the parentheses. ___1___s
by default return ___3___ if you don't specify the value to return. ___2___ can
be standard data types such as string, number, dictionary, tuple, and ___4___
or can be more complicated such as objects and lambda functions.'''
    easy_answers = {
        "___1___": "function",
        "___2___": "arguments",
        "___3___": "None",
        "___4___": "list",
    }
    all_levels = {
        "easy": (easy_string, easy_answers),
    }
    keyorder = ["easy", "medium", "hard"]
    answers = ["easy", "3", "function", "arguments", "None", "list"]
    message = "Fill blank "
    with patch('lib.quiz_game.get_input', side_effect=answers) as result:
        assert play_game(all_levels, message, keyorder) == \
            "All answers are good!"


def test_play_game_user_lose():
    easy_string = """
"A ___1___ is simply another name for a folder. When were working at the
command line, we will refer to them. A computer's files and folders are \
structured like a ___2___. At the beginning is a root ___1___ that ultimately \
branches out to many other folders.  When we navigate our computer's file \
system is effectively walking up and down certain branches of this figurative \
___2___ structure. By default, we'll be starting at the Home folder on our \
computer, indicated by a ___3___ (~) symbol. We'll notice in the shell that a \
cursor appears after a ___4___ sign ($). This is where we will enter \
commands."""
    easy_answers = {
        "___1___": "directory",
        "___2___": "tree",
        "___3___": "tilda",
        "___4___": "dollar",
    }

    medium_string = """Here will be some examples of ___1___s. First ___1___, \
which lists the contents of a directory is ___2___. We can create a new file \
with a specified extension or file type with ___3___ ___1___. But to create a \
new directory we must use ___4___ ___1___."""
    medium_answers = {
        "___1___": "command",
        "___2___": "ls",
        "___3___": "touch",
        "___4___": "mkdir",
    }

    hard_string = """
A ___1___s just like lists are sequences of immutable Python objects. The \
___1___s cannot be changed unlike lists and ___1___s use parentheses, whereas \
lists use square brackets. Python includes built-in methods to manipulate \
strings. One of them is method ___2___(). This method returns a copy of the \
string in which first characters of all the words are capitalized. A similar \
method is ___3___(), which returns a copy of the string with only its FIRST \
character capitalized. When we want all case-based characters to have been \
lowercased we must used method ___4___(). All this methods we use as follows: \
string_name.method_name() ."""
    hard_answers = {
        "___1___": "tuple",
        "___2___": "title",
        "___3___": "capitalize",
        "___4___": "lower",
    }

    all_levels = {
        "easy": (easy_string, easy_answers),
        "medium": (medium_string, medium_answers),
        "hard": (hard_string, hard_answers),
    }

    keyorder = ["easy", "medium", "hard"]
    answers = ["hard", "1", "function", "tuple", "title", "title"]
    message = "Fill blank "
    with patch('lib.quiz_game.get_input', side_effect=answers) as result:
        assert play_game(all_levels, message, keyorder) == \
            "You used all wrong guesses, you lose."
