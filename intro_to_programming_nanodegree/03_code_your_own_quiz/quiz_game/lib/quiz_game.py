# I made function get_input() because is easier to test it with pytest.
def get_input(prompt):
    """
    Takes prompt as input and returns user input. Prompt must be string, and
    output is also string.
    """
    return raw_input(prompt)


def answer_for_blank(blank, answers_dictionary):
    """
    Inputs: string blank and dictionary with answers. Dictionary key:value is
    blank:answer. Output: If blank is in dictionary, output will be the right
    answer(string) for it, if not procedure will return None.
    """
    if blank in answers_dictionary:
        return answers_dictionary[blank]


def how_many_wrong_guesses():
    """
    Output: number of guesses. User write how many guesses want before lose.
    Converts user input to integer, if can't raise ValueError. Infinite tries
    to write right integer number.
    """
    prompt = "How many wrong guesses do you want before lose? "
    while True:
        try:
            wrong_guesses = int(get_input(prompt))
            if wrong_guesses > 0:
                return wrong_guesses
            else:
                raise ValueError
        except ValueError:
            print "You must write integer greater than 0."


def check_answer(user_answer, right_answer):
    """
    Inputs: strings user_answer and right_answer. Output: right_answer or
    Exception. Return right_answer if user_answer is equal to right_answer.
    If not raise Exception.
    """
    if right_answer.lower() in user_answer.lower():
        return right_answer
    raise Exception("Wrong answer.")


def check_answer_with_guesses(wrong_guesses, message, blank, answer,
                              answered_blanks):
    """
    Inputs: strings message, blank, answer, integer wrong_guesses and list
    answered_blanks. Output: wrong_guesses. Message will print when user will
    have to write answer. Procedure will take answer from user and check if
    this answer is right. If right, blank will be added to answered_blanks.
    User can write answers until wrong_guesses >= 1.
    """
    while wrong_guesses >= 0:
        try:
            user_answer = get_input(message + blank + ": ")
            if check_answer(user_answer, answer):
                answered_blanks.append(blank)
                print "Good job! Right answer is:", \
                    check_answer(user_answer, answer) + ".\n"
                return wrong_guesses
        except Exception as e:
            wrong_guesses -= 1
            print e, "\n"
    return wrong_guesses


def play_level(string_task, answers_dictionary, message):
    """
    Inputs: string_task, message and dictionary answers_dictionary. Output:
    string. Procedure takes string with blank spaces. For every blank space in
    string ask user to write right answer. Check if answer is right in
    answers_dictionary. If is, added blank to list answered_blanks and goes to
    next blank space. If all answers are right, returns string "All answers are
    good!". If not, user has just as much guesses as typed.
    """
    blanks_to_answer = answers_dictionary.keys()
    blanks_to_answer.sort()
    answered_blanks = []
    wrong_guesses = how_many_wrong_guesses()
    print string_task
    for element in blanks_to_answer:
        answer = answer_for_blank(element, answers_dictionary)
        if answer:
            wrong_guesses = check_answer_with_guesses(wrong_guesses, message,
                                                      element, answer,
                                                      answered_blanks)
            string_task = string_task.replace(element, answer)
            print string_task
    if blanks_to_answer == answered_blanks:
        return "All answers are good!"
    else:
        return "You used all wrong guesses, you lose."


def check_level(user_select, levels):
    """
    Takes string user_select and list (with possible levels). If user_select is
    in levels returns level, if not raise Exception.
    """
    for level in levels:
        if user_select.lower() == level:
            return level
    raise Exception("Wrong level.")


def select_level(levels):
    """
    Output: level selected by user. This procedure is checking user_input as
    long as it will be proper.
    """
    prompt = "Select game level: " + ", ".join(levels) + " "
    while True:
        try:
            user_select = check_level(get_input(prompt), levels)
            break
        except Exception as message:
            print message, "Please select again.\n"
    return user_select


def order_of_levels(levels_dictionary, keyorder):
    """
    Inputs: dictionary with all levels. Output: list with levels names sorted\
    with given key.
    """
    levels_list = sorted(levels_dictionary.keys(), key=lambda
                         i: keyorder.index(i))
    return levels_list


def play_game(all_levels, message, levels_keyorder):
    """
    Input: dictionary with levels, list levels_keyorder and string message.
    Output: printed message about winnings. All_levels keys will be levels
    (easy, medium and hard.) Levels value will be tuple: (string_task,
    answers_dictionary).  Answers_dictionary key:value is blank:answer.
    """
    levels = order_of_levels(all_levels, levels_keyorder)
    level_string, level_answers = all_levels[select_level(levels)]
    result = play_level(level_string, level_answers, message)
    print result
    return result


easy_string = "A ___1___ is simply another name for a folder. When were \
working at the command line, we will refer to them. A computer's files and \
folders are structured like a ___2___. At the beginning is a root ___1___ \
that ultimately branches out to many other folders.  When we navigate our \
computer's file system is effectively walking up and down certain branches of \
this figurative ___2___ structure. By default, we'll be starting at the Home \
folder on our computer, indicated by a ___3___ (~) symbol. We'll notice in \
the shell that a cursor appears after a ___4___ sign ($). This is where we \
will enter commands."
easy_answers = {
    "___1___": "directory",
    "___2___": "tree",
    "___3___": "tilda",
    "___4___": "dollar",
}

medium_string = "Here will be some examples of ___1___s. First ___1___, which \
lists the contents of a directory is ___2___. We can create a new file with a \
specified extension or file type with ___3___ ___1___. But to create a new \
directory we must use ___4___ ___1___."
medium_answers = {
    "___1___": "command",
    "___2___": "ls",
    "___3___": "touch",
    "___4___": "mkdir",
}

hard_string = "A ___1___s just like lists are sequences of immutable Python \
objects. The ___1___s cannot be changed unlike lists and ___1___s use \
parentheses, whereas lists use square brackets. Python includes built-in \
methods to manipulate strings. One of them is method ___2___(). This method \
returns a copy of the string in which first characters of all the words are \
capitalized. A similar method is ___3___(), which returns a copy of the string\
with only its FIRST character capitalized. When we want all case-based \
characters to have been lowercased we must used method ___4___(). All this \
methods we use as follows: string_name.method_name() ."
hard_answers = {
    "___1___": "tuple",
    "___2___": "title",
    "___3___": "capitalize",
    "___4___": "lower",
}

all_levels = {
    "easy": (easy_string, easy_answers),
    "medium": (medium_string, medium_answers),
    "hard": (hard_string, hard_answers),
}

message = "Fill blank "
keyorder = ["easy", "medium", "hard"]
play_game(all_levels, message, keyorder)
