import pytest
import random
from lib import random_verb
from mock import patch


def test_random_verb():
    with patch('process_madlib.randint', return_value=0) as result:
        assert random_verb() == "run"
    with patch('process_madlib.randint', return_value=1) as result:
        assert random_verb() == "kayak"
