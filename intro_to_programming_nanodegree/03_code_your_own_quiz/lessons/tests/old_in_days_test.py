import pytest
from lib import isLeapYear
from lib import nextDay
from lib import dateIsBefore
from lib import daysBetweenDates
from lib import daysInMonth


def test_daysInMonth():
    assert daysInMonth(2016, 2) == 29
    assert daysInMonth(2017, 2) == 28
    assert daysInMonth(2017, 11) == 30
    assert daysInMonth(2017, 7) == 31


def test_isLeapYear():
    assert isLeapYear(2016) is True
    assert isLeapYear(2017) is False
    assert isLeapYear(2100) is False
    assert isLeapYear(1994) is False
    assert isLeapYear(2000) is True


def test_nextDay():
    assert nextDay(1999, 12, 30) == (1999, 12, 31)
    assert nextDay(2013, 1, 31) == (2013, 2, 1)
    assert nextDay(2012, 12, 30) == (2012, 12, 31)


def test_dateIsBefore():
    assert dateIsBefore(2012, 9, 30, 2012, 10, 30) is True
    assert dateIsBefore(2012, 9, 30, 2011, 10, 30) is False
    assert dateIsBefore(2012, 9, 30, 2012, 8, 30) is False
    assert dateIsBefore(2012, 9, 30, 2012, 9, 12) is False
    assert dateIsBefore(2012, 1, 1, 2013, 1, 1) is True


def test_daysBetweenDates():
    assert daysBetweenDates(2012, 9, 30, 2012, 10, 30) == 30
    assert daysBetweenDates(2011, 6, 30, 2012, 6, 30) == 366
    assert daysBetweenDates(2012, 1, 1, 2012, 2, 28) == 58
    assert daysBetweenDates(2011, 1, 1, 2012, 8, 8) == 585
    assert daysBetweenDates(1900, 1, 1, 1999, 12, 31) == 36523
    with pytest.raises(AssertionError):
        daysBetweenDates(2013, 9, 1, 2012, 9, 4)
