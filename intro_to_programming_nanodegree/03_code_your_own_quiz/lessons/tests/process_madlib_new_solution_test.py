import pytest
from lib import word_in_pos
from lib import play_game


def test_word_in_pos():
    test_cases = ["NOUN", "FALSE", "<<@PERSON><", "PLURALNOUN"]
    parts_of_speech = ["PERSON", "PLURALNOUN", "NOUN"]
    assert word_in_pos(test_cases[0], parts_of_speech) == "NOUN"
    assert word_in_pos(test_cases[1], parts_of_speech) is None
    assert word_in_pos(test_cases[2], parts_of_speech) == "PERSON"
    assert word_in_pos(test_cases[3], parts_of_speech) == "PLURALNOUN"


def test_play_game():
    parts_of_speech = ["PLACE", "PERSON", "PLURALNOUN", "NOUN"]
    test_string = "This is PLACE, no NOUN named PERSON, We have so many "\
        "PLURALNOUN around here."
    result = "This is corgi, no corgi named corgi, We have so many corgi "\
        "around here."
    assert play_game(test_string, parts_of_speech) == result
