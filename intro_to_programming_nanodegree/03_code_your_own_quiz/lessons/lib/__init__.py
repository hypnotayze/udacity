from .process_madlib import random_verb
from .process_madlib_new_solution import word_in_pos
from .process_madlib_new_solution import play_game
from .old_in_days import isLeapYear
from .old_in_days import nextDay
from .old_in_days import dateIsBefore
from .old_in_days import daysBetweenDates
from .old_in_days import daysInMonth
