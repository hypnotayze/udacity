# Here's another chance to practice your for loop skills. Write code for the
# function word_in_pos (meaning word in parts_of_speech), which takes in a
# string word and a list parts_of_speech as inputs. If there is a word in
# parts_of_speech that is a substring of the variable word, then return that
# word in parts_of_speech, else return None.

# Write code for the function play_game, which takes in as inputs
# parts_of_speech (a list of acceptable replacement words) and ml_string (a
# string that can contain replacement words that are found in parts_of_speech).
# Your play_game function should return the joined list replaced, which will
# have the same structure as ml_string, only that replacement words are swapped
# out with "corgi", since this program cannot replace those words with user
# input.


def word_in_pos(word, parts_of_speech):
    # your code here
    for element in parts_of_speech:
        if element in word:
            return element
    return None


def play_game(ml_string, parts_of_speech):
    replaced = []
    # your code here
    ml_list = ml_string.split()
    for element in ml_list:
        word = word_in_pos(element, parts_of_speech)
        if word:
            replaced_string = element.replace(word, "corgi")
            replaced.append(replaced_string)
        else:
            replaced.append(element)
    return " ".join(replaced)
