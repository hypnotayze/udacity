# Given your birthday and the current date, calculate your age
# in days. Compensate for leap days. Assume that the birthday
# and current date are correct dates (and no time travel).
# Simply put, if you were born 1 Jan 2012 and todays date is
# 2 Jan 2012 you are 1 day old.

# IMPORTANT: You don't need to solve the problem yet!
# Just brainstorm ways you might approach it!


def isLeapYear(year):
    ##
    # Your code here. Return True or False
    # Pseudo code for this algorithm is found at
    # http://en.wikipedia.org/wiki/Leap_year#Algorithm
    ##
    if year % 4 != 0:
        return False
    if year % 100 != 0:
        return True
    elif year % 400 != 0:
        return False
    else:
        return True


def daysInMonth(year, month):
    """
    Returns days for month. With including if year is leap year.
    """
    if month == 2:
        if isLeapYear(year):
            return 29
        else:
            return 28
    if month == 1 or month == 3 or month == 5 or month == 7 or month == 8 \
            or month == 10 or month == 12:
        return 31
    else:
        return 30


# Define a simple nextDay procedure, that assumes
# every month has 30 days.
#
# For example:
#   nextDay(1999, 12, 30) => (2000, 1, 1)
#   nextDay(2013, 1, 30) => (2013, 2, 1)
#   nextDay(2012, 12, 30) => (2013, 1, 1)  (even though December really has
#       31 days)
def nextDay(year, month, day):
    """
    Returns the year, month, day of the next day.
    Simple version: assume every month has 30 days.
    """
    # YOUR CODE HERE
    if day < daysInMonth(year, month):
        day = day + 1
    else:
        day = 1
        if month < 12:
            month = month + 1
        else:
            month = 1
            year = year + 1
    return year, month, day


def dateIsBefore(year1, month1, day1, year2, month2, day2):
    # A helper procedure for daysBetweenDates to check if date2 is
    # after date1. If is return True, if isn't return False.
    if year1 < year2:
        return True
    if year1 == year2:
        if month1 < month2:
            return True
        if month1 == month2:
            return day1 <= day2
    return False


def daysBetweenDates(year1, month1, day1, year2, month2, day2):
    # Define a daysBetweenDates procedure that would produce the
    # correct output if there was a correct nextDay procedure.
    # Note that this will NOT produce correct outputs yet, since
    # our nextDay procedure assumes all months have 30 days
    # (hence a year is 360 days, instead of 365).
    # program defensively! Add an assertion if the input is not valid!
    assert not dateIsBefore(year2, month2, day2, year1, month1, day1)
    next_year, next_month, next_day = nextDay(year1, month1, day1)
    days = 0
    while dateIsBefore(next_year, next_month, next_day, year2, month2, day2):
        days += 1
        next_year, next_month, next_day = nextDay(next_year, next_month,
                                                  next_day)
    return days
